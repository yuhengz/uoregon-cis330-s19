#include "cipher.h"

#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}
string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here
    //string a = "nopqrstuvwxyz abcdefghijklm";
    for(unsigned int i = 0; i < enc.size(); i++){
        unsigned int pos;
        bool upper = false;
        for(unsigned int j = 0; j < enc.size(); j++){
            if(enc[i] == ' '){
                pos = 26;
            }
            else if(enc[i] >= 'a'){
                if(enc[i] == smile->cipherText[j]){
                    pos = j;
                }
            }else{
                if(tolower(enc[i]) == smile->cipherText[j]){
                    pos = j;
                    upper = 1;
                }
            }

        }
        if(upper){
            retStr += UPPER_CASE(Cipher().smile->cipherText[pos]);
        }else{
            retStr += Cipher().smile->cipherText[pos];
        }

    }

    cout << "Done" << endl;

    return retStr;
}




struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    // Fill in code here
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
    CaesarSmile->rot = 0;
}

CaesarCipher::CaesarCipher(string in, int rot)
{
    // Fill in code here
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = in;
    CaesarSmile->rot = rot;
}

string CaesarCipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    // Fill in code here
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        pos = (pos + CaesarSmile->rot + 27)%27;
        //cout << pos << endl;
        if(upper) {
            retStr += UPPER_CASE(CaesarSmile->cipherText[pos]);
        } else {
            retStr += CaesarSmile->cipherText[pos];
        }
    }


    cout << "Done" << endl;

    return retStr;

}

string CaesarCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here
    for(unsigned int i = 0; i < enc.size(); i++){
        unsigned int pos;
        bool upper = false;      
        for(unsigned int j = 0; j < enc.size(); j++){
            if(enc[i] == ' '){
                if(enc[i] == CaesarSmile->cipherText[j]){
                    pos = j;
            }
            }
            else if(enc[i] >= 'a'){
                if(enc[i] == CaesarSmile->cipherText[j]){
                    pos = j;
                }
            }else {
                if(enc[i] == toupper(CaesarSmile->cipherText[j])){
                    pos = j;
                    upper = 1;
                }
            }

        }
        pos = (pos - CaesarSmile->rot + 270000000) % 27;
        if(upper){
            retStr += UPPER_CASE(CaesarCipher().smile->cipherText[pos]);
        }else{
            retStr += CaesarCipher().smile->cipherText[pos];
        }

    }

    cout << "Done" << endl;

    return retStr;
}

CaesarCipher &CaesarCipher::operator++() {
    CaesarSmile->rot += 1;
    CaesarSmile->rot = CaesarSmile->rot%27;
    return *this;
}

CaesarCipher &CaesarCipher::operator--() {
    CaesarSmile->rot -= 1;
    CaesarSmile->rot = (CaesarSmile->rot+270000000)%27;
    return *this;
}


